<?php

/**
 * @file
 * Post update functions for the eloqua_api_redux module.
 */

/**
 * Add os_platform field.
 */
function eloqua_api_redux_post_update_move_tokens(&$sandbox) {
  $config_factory = \Drupal::configFactory();
  $config = $config_factory->getEditable('eloqua_api_redux.tokens');

  // If no config, then nothing to move.
  if (count($config->getOriginal()) === 0) {
    return;
  }
  $state = \Drupal::state();
  $config_keys = ['access_token', 'refresh_token', 'api_base_uri'];

  foreach ($config_keys as $key) {
    $config_value = $config->get($key);
    $value = unserialize($config_value, ['allowed_classes' => FALSE]);
    $state_key = 'eloqua_api_redux.' . $key;
    $stateValue = [
      'value' => $value['value'],
      'expire' => $value['expire'],
    ];

    $state->set($state_key, $stateValue);
  }

  // Delete the deprecated configuration.
  $config->delete();
}
