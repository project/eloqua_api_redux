<?php

namespace Drupal\eloqua_api_redux\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\eloqua_api_redux\Service\EloquaApiClient;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Eloqua API Settings.
 */
class Settings extends ConfigFormBase {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Eloqua API Client.
   *
   * @var \Drupal\eloqua_api_redux\Service\EloquaApiClient
   */
  protected $eloquaApiClient;

  /**
   * Settings constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config Factory.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typedConfigManager
   *   Config Factory.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   Date Formatter.
   * @param \Drupal\eloqua_api_redux\Service\EloquaApiClient $eloquaApiClient
   *   Drupal\eloqua_api_redux\Service\EloquaApiClient.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    TypedConfigManagerInterface $typedConfigManager,
    DateFormatterInterface $date_formatter,
    EloquaApiClient $eloquaApiClient,
  ) {
    parent::__construct($config_factory, $typedConfigManager);
    $this->dateFormatter = $date_formatter;
    $this->eloquaApiClient = $eloquaApiClient;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('date.formatter'),
      $container->get('eloqua_api_redux.client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'eloqua_api_redux_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['eloqua_api_redux.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('eloqua_api_redux.settings');
    $refreshToken = $this->eloquaApiClient->getEloquaToken('refresh_token');

    $form['client'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Client Settings'),
    ];

    $form['client']['help'] = [
      '#type' => '#markup',
      '#markup' => $this->t('To get your Client ID, you need to register your application. See details on @link.',
        [
          '@link' => Link::fromTextAndUrl('https://docs.oracle.com/en/cloud/saas/marketing/eloqua-rest-api/Authentication_Auth.html',
            Url::fromUri('https://docs.oracle.com/en/cloud/saas/marketing/eloqua-rest-api/Authentication_Auth.html'))->toString(),
        ]),
    ];

    $form['client']['client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client ID'),
      '#default_value' => $config->get('client_id'),
    ];

    $form['client']['client_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client Secret'),
      '#default_value' => $config->get('client_secret'),
      '#size' => 120,
    ];

    if ($config->get('client_id') != '' && $config->get('client_secret') != '') {
      // Just check if any of the tokens are set, if not set a message.
      if (!is_array($refreshToken)) {
        $msg = $this->t('Tokens are not set. @link. (You will be returned to this page.)', ['@link' => Link::fromTextAndUrl('Click to log into Eloqua and get your tokens', Url::fromUri($this->accessUrl()))->toString()]);
        $this->messenger()->addError($msg);
      }

      // @todo Figure out a nicer way to display the link. Maybe a button?
      $form['client']['tokens'] = [
        '#type' => 'details',
        '#title' => $this->t('Access and Refresh Tokens'),
        '#open' => TRUE,
        '#access' => TRUE,
      ];

      $accessToken = $this->eloquaApiClient->getEloquaToken('access_token');
      if (is_array($accessToken) && array_key_exists('expire', $accessToken)) {
        $form['client']['tokens']['access_expires'] = [
          '#type' => '#markup',
          '#markup' => $this->t(
            'Access token expires: @expires.<br><small>(Access tokens expire after 8 hours.)</small><br><br>',
            [
              '@expires' => $this->dateFormatter->format(
                $accessToken['expire'],
                'long'
              ),
            ]
          ),
        ];
      }

      if (is_array($refreshToken) && array_key_exists('expire', $refreshToken)) {
        $form['client']['tokens']['refresh_expires'] = [
          '#type' => '#markup',
          '#markup' => $this->t(
            'Refresh token expires: @expires.<br><small>(Refresh tokens expire after 1 year.)</small><br><br>',
            [
              '@expires' => $this->dateFormatter->format(
                $refreshToken['expire'],
                'long'
              ),
            ]
          ),
        ];
      }

      $form['client']['tokens']['refresh'] = [
        '#type' => '#markup',
        '#markup' => $this->t('To refresh your tokens, @link. (You will be returned to this page.)', ['@link' => Link::fromTextAndUrl('click to log into Eloqua', Url::fromUri($this->accessUrl()))->toString()]),
      ];

    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('eloqua_api_redux.settings')
      ->set('client_id', $form_state->getValue('client_id'))
      ->set('client_secret', $form_state->getValue('client_secret'))
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Generate the Access Url.
   *
   * See details at
   * https://docs.oracle.com/en/cloud/saas/marketing/eloqua-rest-api/Authentication_Auth.html.
   *
   * @return string
   *   URL.
   */
  private function accessUrl() {
    $config = $this->config('eloqua_api_redux.settings');
    $redirectUrl = Url::fromUri('internal:/eloqua_api_redux/callback', ['absolute' => TRUE])->toString();
    $urlBase = $config->get('api_uri') . 'authorize';

    $query = [
      'response_type' => 'code',
      'client_id' => $config->get('client_id'),
      'redirect_uri' => $redirectUrl,
    ];

    $url = Url::fromUri($urlBase, [
      'query' => $query,
      'absolute' => TRUE,
    ])->toString();

    return $url;
  }

}
