<?php

namespace Drupal\eloqua_api_redux\Service;

/**
 * Interface for Eloqua API Client Service.
 *
 * @package Drupal\eloqua_api_redux\Service
 */
interface EloquaApiClientInterface {

  /**
   * Fetch Eloqua API Access Token by Auth Code.
   *
   * Use the Grant Token to obtain an Access Token and Refresh
   * Token using a POST request to the login.eloqua.com/auth/oauth2/token
   * endpoint.
   *
   * @param string $code
   *   Grant Token (which is in this case an Authorization Code).
   *
   * @return string|bool
   *   The authorization server validates the authorization code and if valid
   *   responds with a JSON body containing the Access Token, Refresh Token,
   *   access token expiration time, and token type
   */
  public function getAccessTokenByAuthCode($code);

  /**
   * Fetch Eloqua API Access Token by Refresh Token.
   *
   * If the access token has expired, you should send your stored Refresh Token
   * to login.eloqua.com/auth/oauth2/token to obtain new tokens.
   *
   * @return bool|mixed
   *   If the request is successful, the response is a JSON body containing
   *   a new access token, token type, access token expiration time, and
   *   new refresh token
   */
  public function getAccessTokenByRefreshToken();

  /**
   * Do the Token Request.
   *
   * @param array $params
   *   Options to pass for Guzzle Request to Eloqua.
   * @param string $res
   *   Resource to call.
   *
   * @return bool|mixed
   *   If the request is successful, the response is a JSON body containing
   *   a new access token, token type, access token expiration time, and
   *   new refresh token.
   */
  public function doTokenRequest(array $params, $res = 'token');

  /**
   * Get Base URL.
   *
   * @return bool|string
   *   Base Url.
   */
  public function getBaseUrl();

  /**
   * Get Eloqua API token from Drupal State.
   *
   * @param string $key
   *   Token label.
   *
   * @return string|false
   *   Cache result.
   */
  public function getEloquaToken($key);

  /**
   * Make the request to Eloqua.
   *
   * @param string $verb
   *   GET POST etc.
   * @param string $endpoint
   *   Endpoint to call.
   * @param array|null $body
   *   Body for request.
   * @param array|null $queryParams
   *   Additional Params to pass.
   *
   * @return array
   *   Results.
   */
  public function doEloquaApiRequest($verb, $endpoint, $body = NULL, $queryParams = NULL);

}
