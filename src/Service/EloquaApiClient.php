<?php

namespace Drupal\eloqua_api_redux\Service;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Http\ClientFactory;
use Drupal\Core\Link;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\State\State;
use Drupal\Core\Url;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Class Eloqua API Client Service.
 *
 * @package Drupal\eloqua_api_redux\Service
 */
class EloquaApiClient implements EloquaApiClientInterface {

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Uneditable Config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The HTTP client to fetch the API data.
   *
   * @var \Drupal\Core\Http\ClientFactory
   */
  protected $httpClientFactory;

  /**
   * State storage.
   *
   * @var \Drupal\Core\State\State
   */
  protected $state;

  /**
   * Eloqua auth fallback service.
   *
   * @var \Drupal\eloqua_api_redux\Service\EloquaAuthFallbackInterface
   */
  protected $authFallbackDefault;

  /**
   * Callback Controller constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config
   *   An instance of ConfigFactory.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   LoggerChannelFactoryInterface.
   * @param \Drupal\Core\Http\ClientFactory $httpClientFactory
   *   A Guzzle client object.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   Time service.
   * @param \Drupal\Core\State\State $state
   *   Drupal\Core\State\State.
   * @param \Drupal\eloqua_api_redux\Service\EloquaAuthFallbackInterface $authFallbackDefault
   *   Eloqua auth fallback service.
   */
  public function __construct(
    ConfigFactory $config,
    LoggerChannelFactoryInterface $loggerFactory,
    ClientFactory $httpClientFactory,
    TimeInterface $time,
    State $state,
    EloquaAuthFallbackInterface $authFallbackDefault,
  ) {
    $this->config = $config->get('eloqua_api_redux.settings');
    $this->loggerFactory = $loggerFactory;
    $this->httpClientFactory = $httpClientFactory;
    $this->time = $time;
    $this->state = $state;
    $this->authFallbackDefault = $authFallbackDefault;
  }

  /**
   * {@inheritdoc}
   */
  public function getAccessTokenByAuthCode($code) {
    if ($accessToken = $this->getValidEloquaToken('access_token')) {
      return $accessToken;
    }

    $params = [
      'redirect_uri' => Url::fromUri('internal:/eloqua_api_redux/callback', ['absolute' => TRUE])->toString(),
      'grant_type' => 'authorization_code',
      'code' => $code,
    ];

    $token = $this->doTokenRequest($params);

    if (is_array($token) && array_key_exists('access_token', $token)) {
      return $token['access_token'];
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getAccessTokenByRefreshToken() {
    if ($accessToken = $this->getValidEloquaToken('access_token')) {
      return $accessToken;
    }

    // Only do a request if we have a valid refresh token.
    if ($refreshToken = $this->getValidEloquaToken('refresh_token')) {
      // @todo Add better handling for expired refresh tokens.
      $params = [
        'redirect_uri' => Url::fromUri('internal:/eloqua_api_redux/callback', ['absolute' => TRUE])->toString(),
        'grant_type' => 'refresh_token',
        'refresh_token' => $refreshToken,
      ];

      $token = $this->doTokenRequest($params);

      if (is_array($token) && array_key_exists('access_token', $token)) {
        return $token['access_token'];
      }
    }
    else {
      // If both access and refresh tokens are expired/invalid use fallback
      // method to generate access and refresh tokens using resource owner
      // password grant authorization.
      // Also make sure that auth fallback service implementation exists.
      $tokenGeneratorService = $this->authFallbackDefault;
      if ($tokenGeneratorService !== NULL) {
        $response = $tokenGeneratorService->generateTokensByResourceOwner();
        if ($response === TRUE && $accessToken = $this->getValidEloquaToken('access_token')) {
          return $accessToken;
        }
      }

      $context = [
        'link' => Link::createFromRoute('Eloqua API Settings', 'eloqua_api_redux.settings')->toString(),
      ];
      $this->loggerFactory->get('eloqua_api_redux')
        ->error(
          "The refresh token is expired. Update tokens by visiting the Eloqua API Settings page.",
          $context
        );
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function doTokenRequest(array $params, $res = 'token') {
    // Guzzle Client.
    $guzzleClient = $this->httpClientFactory->fromOptions([
      'base_uri' => $this->config->get('api_uri'),
    ]);

    $allParams = [
      'form_params' => $params,
      'auth' => [
        $this->config->get('client_id'),
        $this->config->get('client_secret'),
      ],
    ];

    try {
      $response = $guzzleClient->request('POST', $res, $allParams);

      if ($response->getStatusCode() == 200) {
        $contents = $response->getBody()->getContents();
        // @todo Add debugging options.
        $contentsDecoded = Json::decode($contents);

        // @todo Tokens are saved in config as a form of persistent storage.
        $this->setEloquaToken('access_token', $contentsDecoded['access_token']);
        $this->setEloquaToken('refresh_token', $contentsDecoded['refresh_token']);

        // Also update the base urls.
        $this->doBaseUrlRequest();

        return $contentsDecoded;
      }
    }
    catch (GuzzleException $e) {
      // @todo Add debugging options.
      // @todo Add better handling for expired refresh & access tokens.
      $this->loggerFactory->get('eloqua_api_redux')->debug("doTokenRequest() error: @message", ['@message' => $e->getMessage()]);
      return FALSE;
    }

    return FALSE;
  }

  /**
   * Get Cache Age.
   *
   * Access Tokens expire in 8 hours
   * Refresh Tokens expire in 1 year
   * Refresh Tokens will expire immediately after being used to obtain new
   * tokens, or after 1 year if they are not used to obtain new tokens.
   *
   * @param string $key
   *   What type of token is it?
   *
   * @return int
   *   Token age.
   */
  private function eloquaApiCacheAge($key) {
    $cacheAge = 0;

    // Offset a little so we can refresh before time.
    $offset = 3600;

    // Cache access_tokens and base URLs for same amount of time.
    if ($key == 'access_token' || $key == 'api_base_uri') {
      // Cache for 8 hours.
      $cacheAge = 28800;
    }
    if ($key == 'refresh_token') {
      // Cache for 1 year.
      $cacheAge = 31557600;
    }

    return $cacheAge - $offset;
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseUrl() {
    if ($baseUrl = $this->getValidEloquaToken('api_base_uri')) {
      return $baseUrl;
    }

    // Not found in Cache, lets get it from source.
    $data = $this->doBaseUrlRequest();
    if (is_array($data) && array_key_exists('urls', $data) && array_key_exists('base', $data['urls'])) {
      return $data['urls']['base'];
    }

    return FALSE;
  }

  /**
   * Determining base URLs.
   *
   * Eloqua supports multiple data centers, and the https://login.eloqua.com/id
   * endpoint allows you to interface with Eloqua regardless of where the
   * Eloqua install is located.
   *
   * It's important to validate your base URL before making any API calls.
   * If you don't validate, your API calls may not work. New Eloqua users may
   * be added to different data centers, and some Eloqua instances may
   * periodically move between data centers. There are many cases where the
   * base URL used for API access would change.
   *
   * The https://login.eloqua.com/id endpoint should be used to determine
   * the base URL for your API calls.
   *
   * See more details at:
   * https://docs.oracle.com/en/cloud/saas/marketing/eloqua-rest-api/DeterminingBaseURL.html.
   *
   * @return array
   *   The endpoint, when called using basic authentication or OAuth,
   *   will return details about the URLs you should be using.
   */
  protected function doBaseUrlRequest() {
    // @todo Ideally merge all the Guzzle requests into one generic method.
    // Guzzle Client.
    $guzzleClient = $this->httpClientFactory->fromOptions([
      // @todo Move this into Config?
      'base_uri' => 'https://login.eloqua.com/',
      'headers' => [
        'Authorization' => 'bearer ' . $this->getAccessTokenByRefreshToken(),
      ],
    ]);

    try {
      $response = $guzzleClient->request('GET', 'id', []);
      if ($response->getStatusCode() == 200) {
        $contents = $response->getBody()->getContents();
        // @todo Add debugging options.
        $contentsDecoded = Json::decode($contents);
        if (
          is_array($contentsDecoded)
          && isset($contentsDecoded['urls']['base'])
        ) {
          // @todo Base Urls are saved in config as a form of persistent storage.
          $this->setEloquaToken('api_base_uri', $contentsDecoded['urls']['base']);
        }
        else {
          // @todo Add debugging options.
          // @todo Add better handling for expired refresh & access tokens.
          $this->loggerFactory->get('eloqua_api_redux')->debug("doBaseUrlRequest() error: @message", ['@message' => $contentsDecoded]);
          return [];
        }

        return $contentsDecoded;
      }
    }
    catch (GuzzleException $e) {
      // @todo Add debugging options.
      // @todo Add better handling for expired refresh & access tokens.
      $this->loggerFactory->get('eloqua_api_redux')->debug("doBaseUrlRequest() error: @message", ['@message' => $e->getMessage()]);
      return [];
    }

    return [];
  }

  /**
   * Store Eloqua API token.
   *
   * @param string $key
   *   Cache ID.
   * @param string $value
   *   Cache Value.
   *
   * @return bool
   *   Status.
   */
  protected function setEloquaToken($key, $value) {
    // Add the expiration timestamp to the token.
    $stateValue = [
      'value' => $value,
      'expire' => $this->time->getRequestTime() + $this->eloquaApiCacheAge($key),
    ];

    $stateKey = $this->getStateKey($key);
    // Save the value in Drupal state.
    $this->state->set($stateKey, $stateValue);

    return TRUE;
  }

  /**
   * Get and validate Eloqua API token.
   *
   * @param string $key
   *   Token label.
   *
   * @return string|false
   *   Cache result.
   */
  protected function getValidEloquaToken($key) {
    $value = $this->getEloquaToken($key);
    if (
      is_array($value)
      && array_key_exists('value', $value)
      && array_key_exists('expire', $value)
    ) {
      $now = $this->time->getRequestTime();
      $expire = $value['expire'];

      // Manually validate if the token is still fresh.
      if ($expire > $now) {
        // Return result from cache if found.
        return $value['value'];
      }
    }

    return FALSE;

  }

  /**
   * {@inheritdoc}
   */
  public function getEloquaToken($key) {
    $stateKey = $this->getStateKey($key);

    // Get the value from Drupal state.
    $stateValue = $this->state->get($stateKey);
    if (
      is_array($stateValue)
      && array_key_exists('value', $stateValue)
      && array_key_exists('expire', $stateValue)
    ) {
      return $stateValue;
    }

    return FALSE;
  }

  /**
   * Returns the Drupal State API key.
   *
   * @param string $key
   *   Key name.
   *
   * @return string
   *   The State API key.
   */
  protected function getStateKey($key) {
    return 'eloqua_api_redux.' . $key;
  }

  /**
   * {@inheritdoc}
   */
  public function doEloquaApiRequest($verb, $endpoint, $body = NULL, $queryParams = NULL) {
    // @todo Ideally merge all the Guzzle requests into one generic method.
    // If the base URL is unavailable, don't even try the request.
    if (!$baseUrl = $this->getBaseUrl()) {
      return [];
    }

    // Guzzle Client.
    $guzzleClient = $this->httpClientFactory->fromOptions([
      // @todo Move this into Config?
      'base_uri' => $baseUrl,
      'headers' => [
        'Authorization' => 'bearer ' . $this->getAccessTokenByRefreshToken(),
        'Content-Type' => 'application/json',
      ],
    ]);

    $allParams = [];
    if (!is_null($body)) {
      $allParams['body'] = json_encode($body);
    }
    if (!is_null($queryParams)) {
      $allParams['query'] = $queryParams;
    }

    try {
      $response = $guzzleClient->request($verb, $endpoint, $allParams);
      // See https://docs.oracle.com/en/cloud/saas/marketing/eloqua-rest-api/APIRequests_HTTPStatusCodes.html.
      if ($response->getStatusCode() == 200 || $response->getStatusCode() == 201 || $response->getStatusCode() == 204) {
        $contents = $response->getBody()->getContents();
        // @todo Add debugging options.
        $contentsDecoded = Json::decode($contents);
        return $contentsDecoded;
      }
    }
    catch (GuzzleException $e) {
      // @todo Add debugging options.
      // @todo Add better handling for expired refresh & access tokens.
      $this->loggerFactory->get('eloqua_api_redux')->debug("doEloquaApiRequest() error: @message", ['@message' => $e->getMessage()]);
      return [];
    }

    return [];
  }

}
