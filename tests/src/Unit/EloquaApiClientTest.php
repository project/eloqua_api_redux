<?php

namespace Drupal\Tests\eloqua_api_redux\Unit;

use Drupal\Component\Serialization\Json;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Tests\UnitTestCase;
use GuzzleHttp\Psr7\Response;

/**
 * Implements Eloqua api client service tests.
 *
 * @ingroup eloqua_api_redux
 *
 * @group eloqua_api_redux
 *
 * @coversDefaultClass \Drupal\eloqua_api_redux\Service\EloquaApiClient
 */
class EloquaApiClientTest extends UnitTestCase {

  /**
   * Sample json response from eloqua auth API.
   */
  const ACCESS_TOKEN_RESPONSE = 'accessTokenResponse.json';

  /**
   * Sample json response from eloqua base URL API.
   */
  const BASE_URL_RESPONSE = 'baseUrlResponse.json';

  /**
   * Logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * Config instance.
   *
   * @var \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Token Config instance.
   *
   * @var \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig
   */
  protected $configTokens;

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Guzzle http client factory.
   *
   * @var \Drupal\Core\Http\ClientFactory
   */
  protected $httpClientFactory;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The module handler to load the module path.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * State storage.
   *
   * @var \Drupal\Core\State\State
   */
  protected $state;

  /**
   * Eloqua auth fallback service.
   *
   * @var \Drupal\eloqua_api_redux\Service\EloquaAuthFallbackInterface
   */
  protected $authFallbackDefault;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->httpClientFactory = $this->createMock('Drupal\Core\Http\ClientFactory');

    $this->config = $this->createMock('Drupal\Core\Config\ImmutableConfig');

    $this->configFactory = $this->createMock('Drupal\Core\Config\ConfigFactory');

    $this->configFactory->expects(self::any())
      ->method('get')
      ->with('eloqua_api_redux.settings')
      ->willReturn($this->config);

    $this->configFactory->expects(self::any())
      ->method('getEditable')
      ->with('eloqua_api_redux.tokens')
      ->willReturn($this->configTokens);

    $this->loggerFactory = $this->createMock('Drupal\Core\Logger\LoggerChannelFactoryInterface');

    $this->time = $this->createMock('Drupal\Component\Datetime\TimeInterface');

    $this->moduleHandler = $this->createMock('Drupal\Core\Extension\ModuleHandlerInterface');

    $this->state = $this->createMock('Drupal\Core\State\State');

    $this->authFallbackDefault = $this->createMock('Drupal\eloqua_api_redux\Service\EloquaAuthFallbackInterface');

    // Set container and required dependencies.
    $container = new ContainerBuilder();
    $pathValidator = $this->createMock('Drupal\Core\Path\PathValidatorInterface');
    $unroutedUrlAssembler = $this->createMock('Drupal\Core\Utility\UnroutedUrlAssemblerInterface');
    $container->set('path.validator', $pathValidator);
    $container->set('unrouted_url_assembler', $unroutedUrlAssembler);

    \Drupal::setContainer($container);
  }

  /**
   * Test the getAccessTokenByRefreshToken() method.
   *
   * @covers ::getAccessTokenByRefreshToken
   * @covers ::__construct
   */
  public function testGetAccessTokenByRefreshToken() {
    $tokenResponse = Json::decode($this->getMockResponseContents(self::ACCESS_TOKEN_RESPONSE));
    $refreshToken = 'tGzv3JOkF0XG5Qx2TlKW';

    $eloquaApiClient = $this->getMockBuilder('Drupal\eloqua_api_redux\Service\EloquaApiClient')
      ->setConstructorArgs([
        $this->configFactory,
        $this->loggerFactory,
        $this->httpClientFactory,
        $this->time,
        $this->state,
        $this->authFallbackDefault,
      ])
      ->onlyMethods(['getValidEloquaToken', 'doTokenRequest'])
      ->getMock();

    $eloquaApiClient->expects(self::any())
      ->method('getValidEloquaToken')
      ->willReturnMap([
        ['access_token', NULL],
        ['refresh_token', $refreshToken],
      ]);

    $eloquaApiClient->expects(self::any())
      ->method('doTokenRequest')
      ->willReturn($tokenResponse);

    $response = $eloquaApiClient->getAccessTokenByRefreshToken();
    $this->assertNotNull($response);
  }

  /**
   * Test the getAccessTokenByAuthCode() method.
   *
   * @covers ::getAccessTokenByAuthCode
   * @covers ::__construct
   */
  public function testGetAccessTokenByAuthCode() {
    $tokenResponse = Json::decode($this->getMockResponseContents(self::ACCESS_TOKEN_RESPONSE));
    // cspell:disable-next-line
    $authCode = 'SplxlOBeZQQYbYS6WxSbIA';

    $eloquaApiClient = $this->getMockBuilder('Drupal\eloqua_api_redux\Service\EloquaApiClient')
      ->setConstructorArgs([
        $this->configFactory,
        $this->loggerFactory,
        $this->httpClientFactory,
        $this->time,
        $this->state,
        $this->authFallbackDefault,
      ])
      ->onlyMethods(['getValidEloquaToken', 'doTokenRequest'])
      ->getMock();

    $eloquaApiClient->expects(self::any())
      ->method('getValidEloquaToken')
      ->with('access_token')
      ->willReturn(NULL);

    $eloquaApiClient->expects(self::any())
      ->method('doTokenRequest')
      ->willReturn($tokenResponse);

    $response = $eloquaApiClient->getAccessTokenByAuthCode($authCode);
    $this->assertNotNull($response);
  }

  /**
   * Test the doTokenRequest() method.
   *
   * @covers ::doTokenRequest
   * @covers ::__construct
   */
  public function testDoTokenRequest() {
    $params = [
      'redirect_uri' => 'http://localhost/eloqua_api_redux/callback',
      'grant_type' => 'refresh_token',
      'refresh_token' => 'SplxlOBeZQQYbYS6WxSbIA',
    ];
    $mockedResponse = new Response(200,
      ['Content-Type' => 'application/json'],
      $this->getMockResponseContents(self::ACCESS_TOKEN_RESPONSE)
    );

    $httpClientFactory = $this->createMock('Drupal\Core\Http\ClientFactory');

    $guzzleClient = $this->createMock('GuzzleHttp\Client');

    $guzzleClient->expects(self::any())
      ->method('request')
      ->willReturn($mockedResponse);

    $httpClientFactory->expects(self::any())
      ->method('fromOptions')
      ->willReturn($guzzleClient);

    $eloquaApiClient = $this->getMockBuilder('Drupal\eloqua_api_redux\Service\EloquaApiClient')
      ->setConstructorArgs([
        $this->configFactory,
        $this->loggerFactory,
        $httpClientFactory,
        $this->time,
        $this->state,
        $this->authFallbackDefault,
      ])
      ->onlyMethods(['setEloquaToken', 'doBaseUrlRequest'])
      ->getMock();

    $eloquaApiClient->expects(self::any())
      ->method('setEloquaToken')
      ->willReturnMap([
        ['access_token'],
        ['refresh_token'],
      ]);

    $eloquaApiClient->expects(self::any())
      ->method('setEloquaToken');

    $response = $eloquaApiClient->doTokenRequest($params);
    $this->assertNotEmpty($response);
  }

  /**
   * Test the getBaseUrl() method.
   *
   * @covers ::getBaseUrl
   * @covers ::__construct
   */
  public function testGetBaseUrl() {
    $mockedResponse = new Response(200,
      ['Content-Type' => 'application/json'],
      $this->getMockResponseContents(self::BASE_URL_RESPONSE)
    );
    $baseUrlResponse = Json::decode($this->getMockResponseContents(self::BASE_URL_RESPONSE));

    $httpClientFactory = $this->createMock('Drupal\Core\Http\ClientFactory');

    $guzzleClient = $this->createMock('GuzzleHttp\Client');

    $guzzleClient->expects(self::any())
      ->method('request')
      ->willReturn($mockedResponse);

    $httpClientFactory->expects(self::any())
      ->method('fromOptions')
      ->willReturn($guzzleClient);

    $eloquaApiClient = $this->getMockBuilder('Drupal\eloqua_api_redux\Service\EloquaApiClient')
      ->setConstructorArgs([
        $this->configFactory,
        $this->loggerFactory,
        $httpClientFactory,
        $this->time,
        $this->state,
        $this->authFallbackDefault,
      ])
      ->onlyMethods(['getValidEloquaToken', 'doBaseUrlRequest'])
      ->getMock();

    $eloquaApiClient->expects(self::any())
      ->method('doBaseUrlRequest')
      ->willReturn($baseUrlResponse);

    $response = $eloquaApiClient->getBaseUrl();
    $this->assertNotEmpty($response);
    $this->assertEquals('https://secure.p03.eloqua.com', $response);
  }

  /**
   * Gets mock response from the given file.
   *
   * @param string $fileName
   *   File name for mock response.
   *
   * @return string
   *   Mock response contents.
   */
  protected function getMockResponseContents($fileName) {
    return file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . 'Mocks' . DIRECTORY_SEPARATOR . $fileName);
  }

}
